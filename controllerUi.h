#ifndef CONTROLLER_UI_H_
#define CONTROLLER_UI_H_

#define DEBUG 0
#if DEBUG
#define PRINT(...) Serial.print(__VA_ARGS__)
#define PRINTLN(...) Serial.println(__VA_ARGS__)
#define SERIALINIT(x)  Serial.begin(x)
#else
#define PRINT  
#define PRINTLN  
#define SERIALINIT(x)  
#endif

typedef struct{
  int result;
  char data;
}menu_t;

typedef struct{
  int L0;
  int Ln;
  int d0;
  int dn;
  int p0;
  int pn;
}calVal_t;


#endif
