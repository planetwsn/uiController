/**************************************************************************
 This is an example for our Monochrome OLEDs based on SSD1306 drivers

 Pick one up today in the adafruit shop!
 ------> http://www.adafruit.com/category/63_98

 This example is for a 128x64 pixel display using SPI to communicate
 4 or 5 pins are required to interface.

 Adafruit invests time and resources providing this open
 source code, please support Adafruit and open-source
 hardware by purchasing products from Adafruit!

 Written by Limor Fried/Ladyada for Adafruit Industries,
 with contributions from the open source community.
 BSD license, check license.txt for more information
 All text above, and the splash screen below must be
 included in any redistribution.
 **************************************************************************/
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Keypad.h>
#include <EEPROM.h>
#include "controllerUi.h"

#define MIN_DISP_UPDATE_INTERVAL  500

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Use hardware SPI
#define OLED_CS     3
#define OLED_DC     4
#define OLED_RESET  5

#define ROWS 4
#define COLS 4

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT,
  &SPI, OLED_DC, OLED_RESET, OLED_CS);

#define NUMFLAKES     10 // Number of snowflakes in the animation example

#define LOGO_HEIGHT   16
#define LOGO_WIDTH    16

char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte rowPins[ROWS] = {13,12,11,10};
byte colPins[COLS] = {9,8,7,6};
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);


char val[5], v;
unsigned vi,trackMenu;
long unsigned lastDisplayUpdateTime, lastTimerUpdate;
bool openMenu;
int configVal[10];

void setup() {
  SERIALINIT(9600);
  
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC)) {
    PRINTLN(F("[SETUP]SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  //Setup keypad events
  keypad.addEventListener(keypadCallback);
  welcomeScreen();
  
  //retrieve saved config
  for(int j = 0; j < 10; j++){
    configVal[j]=getConfig(2*j);
  }
  
  openMenu = false;
  lastDisplayUpdateTime = millis();
  lastTimerUpdate = millis();
  idleScreen();
}

void loop() {
  char k = keypad.getKey();

  if(openMenu){
    PRINTLN(F("[LOOP]Opening main menu"));
    trackMenu = 0;
    mainMenu('u');    
    PRINTLN(F("[LOOP] Resuming idle loop"));
    openMenu = false;
    idleScreen();
  }
}

void resetSystem(){
  for(int k = 0; k < 10; k++)
    saveConfig(2*k,0);
}

int getConfig(int addr){
  long two = EEPROM.read(addr);
  long one = EEPROM.read(addr + 1);

  return ((two << 0)&0xFF) + ((one << 8)&0xFFFF); 
}

void saveConfig(int addr, int val){
  PRINT("Val: ");PRINTLN(val);
  byte two = (val & 0xFF);
  byte one = ((val >> 8) & 0xFF);

  EEPROM.write(addr, two);
  EEPROM.write(addr + 1, one);
}

void mainMenu(char choice){
  PRINTLN(F("[M.Menu]Init!"));
  switch (choice){
    case '1':
      oledPrint("L0=",1,1,'w',1);
      sprintf(val,"%03i",getConfig(0));
      oledPrint(val,1,4,'a',1);
      oledPrint("Ln=",1,8,'a',1);
      sprintf(val,"%03i",getConfig(2));
      oledPrint(val,1,11,'a',1);
      
      oledPrint("A. Set L0",2,1,'a',1);
      oledPrint("B. Set Ln",3,1,'a',1);
      oledPrint("C. Init cal",4,1,'a',1);
      choice = keypad.waitForKey();
      return mainMenu(choice);
    case '2':
      trackMenu += 4;
      oledPrint("d0=",1,1,'w',1);
      sprintf(val,"%03i",getConfig(4));
      oledPrint(val,1,4,'a',1);
      oledPrint("dn=",1,8,'a',1);
      sprintf(val,"%03i",getConfig(6));
      oledPrint(val,1,11,'a',1);
      
      oledPrint("A. Set d0",2,1,'a',1);
      oledPrint("B. Set dn",3,1,'a',1);
      oledPrint("C. Init cal",4,1,'a',1);
      choice = keypad.waitForKey();
      return mainMenu(choice);   
    case '3':
      trackMenu += 8;
      oledPrint("p0=",1,1,'w',1);
      sprintf(val,"%03i",getConfig(8));
      oledPrint(val,1,4,'a',1); 
      oledPrint("pn=",1,8,'a',1);
      sprintf(val,"%03i",getConfig(10));
      oledPrint(val,1,11,'a',1);
      
      oledPrint("A. Set p0",2,1,'a',1);
      oledPrint("B. Set pn",3,1,'a',1);
      oledPrint("C. Init cal",4,1,'a',1);
      choice = keypad.waitForKey();
      return mainMenu(choice);
    case 'B':
      trackMenu += 2;
    case 'A':
      vi = 0;
      v = '0';
      oledPrint("Input value:",1,1,'w',1);
      do{
        v = input(trackMenu);      
      }while(v != '#' && v != '*' && v != 'D');

      if(v == 'D'){
        oledPrint("Input saved!",8,1,'a',1);
      }else if(v == '#'){
        oledPrint("Input cancelled!",8,1,'a',1);
      }
      delay(1500);
      return;  
    case 'C':
      oledPrint("Calibrating...",1,1,'w',1);
      trackMenu +=10;
      //forward calvalues to Tiva with trackValue
      delay(1500);
      oledPrint("Completed!",3,1,'a',1);
      delay(1500);
      return;
    case '#':
      oledPrint("Cancelling op",1,1,'w',1);
      delay(1500);
      return;
    case 'u':
      oledPrint("Choose an option",1,1,'w',1);
      oledPrint("1. Light sensor",2,1,'a',1);
      oledPrint("2. Displacement s.",3,1,'a',1);
      oledPrint("3. Position sensor",4,1,'a',1);
      do{
        choice = keypad.waitForKey();
      }while(!(choice == '1' || choice == '2' || choice == '3' || choice == '#')); 
      return mainMenu(choice);
    case '@':
      return;
    default:
      oledPrint("Invalid input!",1,1,'w',1);
      delay(1500);
      return;
  }
}

char input(unsigned trackMenu){
    do{
      val[vi] = keypad.waitForKey();
    }while((val[vi] == 'D' && vi == 0) || val[vi] == 'B' || val[vi] == 'C' || val[vi] == 'A' || val[vi] == '*');

   if(val[vi] == '#')
    return val[vi];
   
   else if(val[vi] == 'D'){
    val[vi]='\0';
    saveConfig(trackMenu,atoi(val));
    PRINTLN("[INPUT]Leaving");
    return 'D';
    
   }else if(vi == 3){
    //save input
    val[vi]='\0';
    saveConfig(trackMenu,atoi(val)); 
    return 'D';
   }

   for(int n = 0; n <= vi; n++){
    display.setCursor(space2Pix(n+1,1),line2Pix(3,1));
    display.print(val[n]);
   }
   oledPrint("Press D to save",5,1,'a',1);
   oledPrint("Press # to cancel",6,1,'a',1);
   vi++;
   return 'A';
}

void idleScreen(){
  oledPrint("Calibrator v1.0",1,4,'w',1);
  oledPrint("Press '*' to begin",2,3,'a',1);
}
/*
 * Note: All characters for text size 1 is 8 pixels tall. Set cursor
 * position relevant to this fact. 
 * Formula: positon = (64/8*(line-1))
 */
int16_t line2Pix(int16_t line,int16_t fontsize){
  return (64/8*(line-1)*fontsize);
}

/*
 * Note: All characters for text size 1 is 5 pixels wide plus 1 pixel space. Set cursor
 * position relevant to this fact. 
 * Formula: positon = (128/21*(space-1))
 */
int16_t space2Pix(int16_t spacing,int16_t fontsize){
  return (128/21*(spacing-1)*fontsize);
}

void welcomeScreen(){
  display.clearDisplay();

  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(WHITE);        // Draw white text
  display.setCursor(space2Pix(5,1),line2Pix(5,1));  // Place text center screen
  display.println(F("Welcome, User!"));
  
  display.display();
  delay(3000);
}

void oledPrint(const char* text, int posCol, int posRow, char mode, int fontsize){
  switch(mode){
    case 'w':
      display.clearDisplay();
    case 'a':
      display.setTextSize(fontsize);             // Normal 1:1 pixel scale
      display.setTextColor(WHITE);        // Draw white text
      display.setCursor(space2Pix(posRow,fontsize),line2Pix(posCol,fontsize));  // Place text center screen
      display.println(text);
      break;
    case 'c':
      display.setTextSize(fontsize);
      display.setTextColor(WHITE);
      display.setCursor(space2Pix(posRow,fontsize),line2Pix(posCol,fontsize));
      display.print(text);
  }
  display.display();
}

void keypadCallback(KeypadEvent key){
  switch (keypad.getState()){
    case PRESSED:
        PRINT(F("[PRESSED]key pressed: ")); PRINTLN(key);
        if (key == '*') {
            openMenu = true;
        }
        break;
    case RELEASED:
        break;
    case HOLD:
        if (key == 'p') {
            oledPrint("Resetting config...",1,1,'w',1);    // Blink the LED when holding the * key.
            delay(1500);
            resetSystem();
            oledPrint("Completed!",3,1,'a',1);
            delay(1500);
            idleScreen();
        }
        break;
    case IDLE:
        break;
    default:
        PRINT(F("[DEFAULT]key pressed: ")); PRINTLN(key);
  }
}
